﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using University.webservice;
using System.Web.Security;

namespace University.Admin
{
    public partial class Profile : System.Web.UI.Page
    {
        wsUV1 ws = new wsUV1();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["Name"] == null || Session["ID"] == null)
            {
                Response.Redirect("/login.aspx");
            }

            loadProfileData();
        }


        protected void loadProfileData()
        {

            if (Request.QueryString["uID"].Length > 0) { 
                int uID = Convert.ToInt32(Request.QueryString["uID"]);
                User u = this.ws.GetUserData(uID);

                lbl_profile_data.Text = " Name: " + u.name + "<br>";
                lbl_profile_data.Text += " Surname: " + u.surname + "<br>";
                lbl_profile_data.Text += " Address: " + u.address + "<br>";

                lbl_profile_data.Text += " Login user: " + u.userID + "<br><br>";

                Grade g = this.ws.GetUserGrade(uID); //User has grade or not
                if (g != null)
                {
                    lbl_profile_data.Text += " Grade: " + g.name + "<br>";
                }

                lbl_profile_data.Text += " Current year: " + u.currentYear + "<br>";

                Subject[] subjects;
                subjects = this.ws.GetUserSubjects(uID);
                string output_subjects = "";

                lbl_profile_data.Text += " Subjects: " + "";

                output_subjects = "<a href=\"addSubject.aspx?uID="+uID+ "\">Add subject</a> <br>";
                if (subjects.Length > 0)
                {
                    foreach (Subject s in subjects)
                    {
                        output_subjects += " <a href=\"subject.aspx?uID="+uID+"&sID="+s.ID+"\" >" + s.name +"</a>";
                        output_subjects += "     <a href=\"deleteSubject.aspx?uID=" + uID + "&sID=" + s.ID + "\">Delete</a> ";

                        output_subjects += "<br>";
                    }
               }

                lbl_profile_data.Text += output_subjects + "<br>";


            }


        }



    }
}