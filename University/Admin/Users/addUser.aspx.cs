﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using University.webservice;

namespace University.Admin.Users
{
    public partial class addUser : System.Web.UI.Page
    {
        wsUV1 ws = new wsUV1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                populateGradesDropDown();
            }
        }


        protected void populateGradesDropDown()
        {
            Grade[] grades;
            grades = ws.GetGrades();
            if (grades.Length > 0)
            {

                dpdown_grade.DataSource = grades;
                dpdown_grade.DataTextField = "name";
                dpdown_grade.DataValueField = "ID";
                dpdown_grade.DataBind();
            }
            else
            {
                //Dropdown empty, no grades
                dpdown_grade.Enabled = false;
            }
        }

        protected void btn_newUser_ok_Click(object sender, EventArgs e)
        {

            User u = new User();

            u.name = txbox_name.Text.ToString();
            u.surname = txbox_surname.Text.ToString();
            u.address = txbox_address.Text.ToString();
            u.userID = tbox_newUserName.Text.ToString();

            string psswd = tbox_newUserPsswd.Text.ToString();
            u.pass = psswd;

            u.currentYear = Convert.ToInt32(txbox_cuurentYear.Text);
            u.ID_Grade = Convert.ToInt32(dpdown_grade.SelectedValue);
            u.isTeacher = Convert.ToBoolean(cb_isTeacher.Checked);
            u.isAdmin = false;


            bool succedd = ws.AddUser(u);
            if (succedd) { Response.Redirect("/Admin/controlPage.aspx"); }
            else { lbl_msg.Text = "User creation failed"; }


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}