﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using University.webservice;


namespace University.Admin
{
    public partial class addSubject : System.Web.UI.Page
    {
        wsUV1 ws = new wsUV1();
        int uID, sID;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.uID = Convert.ToInt32(Request.QueryString["uID"]);
            this.sID = Convert.ToInt32(Request.QueryString["sID"]);

            if (!Page.IsPostBack) { 
                loadDropDown(uID);
            }
        }

        protected void btn_addSubject_Click(object sender, EventArgs e)
        {
            this.ws.AddToSubject(this.uID, Convert.ToInt32(dp_subjects.SelectedValue));
            Response.Redirect("Profile.aspx?uID="+this.uID);
        }

        protected void dp_subjects_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void loadDropDown(int uID)
        {
            Subject[] subjects = this.ws.GetRemainingSubjectsForUser(uID);
            dp_subjects.DataSource = subjects;
            dp_subjects.DataValueField = "ID";
            dp_subjects.DataTextField = "Name";
            dp_subjects.DataBind();

        }
    }
}