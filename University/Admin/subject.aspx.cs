﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using University.webservice;
using System.Web.Security;

namespace University.Admin
{
    public partial class subject : System.Web.UI.Page
    {

        wsUV1 ws = new wsUV1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["sID"].ToString().Length > 0)
            {
                int sID = Convert.ToInt32(Request.QueryString["sID"]);
                int uID = Convert.ToInt32(Request.QueryString["uID"]);

                loadSubjectData(sID,uID);
            }
        }



        protected void loadSubjectData(int sID, int uID)
        {
            // Subject info
            Subject s = this.ws.GetSubject(sID);
            if (s != null)
            {
                lbl_subject_data.Text = "";
                lbl_subject_data.Text += "<b>"+s.name+"</b><br>";

                Grade g = this.ws.GetUserGrade(uID);
                lbl_subject_data.Text += "Grade: " + g.name + ", year "+s.year+"<br>";
                lbl_subject_data.Text += s.description + "<br><br>";

            }

        }
    }
}