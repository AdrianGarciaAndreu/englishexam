﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using University.webservice;
using System.Web.Security;

namespace University.Users
{
    public partial class Profile : System.Web.UI.Page
    {

        wsUV1 ws = new wsUV1();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Name"] == null || Session["ID"] == null) { 
                Response.Redirect("/login.aspx"); 
            }

            lbl_profile_name.Text = "Welcome "+Session["Name"].ToString();

            loadProfileData();
        }

        protected void btn_signOut_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Response.Redirect("/home.aspx");
        }




        protected void loadProfileData()
        {
            
            if (Session["ID"] != null) {
                int uID = Convert.ToInt32(Session["ID"]);
                User u = this.ws.GetUserData(uID);

                lbl_profile_data.Text = " Name: "+u.name + "<br>";
                lbl_profile_data.Text += " Surname: " + u.surname + "<br>";
                lbl_profile_data.Text += " Address: " + u.address + "<br>";

                lbl_profile_data.Text += " Login user: " + u.userID + "<br><br>";

                Grade g = this.ws.GetUserGrade(uID); //User has grade or not
                if (g != null){
                    lbl_profile_data.Text += " Grade: " + g.name + "<br>";
                }

                lbl_profile_data.Text += " Current year: " + u.currentYear + "<br>";
                
                Subject[] subjects;
                subjects = this.ws.GetUserSubjects(uID);
                string output_subjects = "";

                lbl_profile_data.Text += " Subjects: " + "<br>";
                if (subjects.Length > 0) { 
                    foreach(Subject s in subjects)
                    {
                        output_subjects += " <a href=\"subject.aspx?uID=" + uID + "&sID=" + s.ID + "\" >" + s.name + "</a>";
                        output_subjects += "<br>";
                    }
                    lbl_profile_data.Text += output_subjects+"<br>";
                }

            }

        }

    }
}