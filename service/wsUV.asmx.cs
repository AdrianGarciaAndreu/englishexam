﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SQLite;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;

namespace service
{
    /// <summary>
    /// Summary description for wsUV1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class wsUV1 : System.Web.Services.WebService
    {

        private string strconn = "Data Source=" + HttpRuntime.AppDomainAppPath.Replace("\\", "/") + "/db/UV.db;Version=3;";



        [WebMethod]
        public int LogIn(string userID, string psswd)
        {

            int _id = -1;
            //string strconn = "Data Source=" + HttpRuntime.AppDomainAppPath.Replace("\\", "/") + "/db/UV.db;Version=3;";
            System.Diagnostics.Debug.WriteLine(strconn);


            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                using (SQLiteDataAdapter adapter = new SQLiteDataAdapter())
                {


                    //Get the pass encrypted
                    using (MD5 md5Hash = MD5.Create())
                    {
                        byte[] pData = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(psswd));

                        //Convert the HEX bytes hassed array to string replacing '.', by empties
                        psswd = BitConverter.ToString(pData).Replace("-", String.Empty);


                        string qry = "SELECT * FROM Users WHERE UserID =  \"" + userID + "\" AND Pass = \"" + psswd + "\";";
                        using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                        {

                            //System.Diagnostics.Debug.WriteLine("QUERY:" + qry);


                            SQLiteDataReader rd1 = cmd.ExecuteReader();
                            DataTable dt = new DataTable();

                            dt.Load(rd1);
                            if (dt.Rows.Count > 0)
                            {
                                //User correct
                                DataRow row = dt.Rows[0];
                                _id = Convert.ToInt32(row["ID"]);
                            }


                        } // End Query


                    } //Password encrypt



                } // End Adapter

                conn.Close();

            }//Connection end

            return _id;
        }



        [WebMethod]
        public User GetUserData(int ID)
        {

            User u = new User();
            //string strconn = "Data Source=" + HttpRuntime.AppDomainAppPath.Replace("\\", "/") + "/db/UV.db;Version=3;";
            System.Diagnostics.Debug.WriteLine(strconn);


            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();



                string qry = "SELECT * FROM Users WHERE ID =" + ID + ";";
                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {

                    SQLiteDataReader rd1 = cmd.ExecuteReader();
                    DataTable dt = new DataTable();

                    dt.Load(rd1);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        u.name = row["Name"].ToString();
                        u.surname = row["Surname"].ToString();
                        u.address = row["Address"].ToString();
                        u.userID = row["UserID"].ToString();
                        u.ID = Convert.ToInt32(row["ID"]);
                        u.pass = row["Pass"].ToString();
                        u.currentYear = Convert.ToInt32(row["CurrentYear"]);
                        u.ID_Grade = Convert.ToInt32(row["ID_Grade"]);
                        u.isTeacher = Convert.ToBoolean(row["isTeacher"]);
                        u.isAdmin = Convert.ToBoolean(row["isAdmin"]);

                    }


                } // End Query




                conn.Close();

            }//Connection end

            return u;
        }



        [WebMethod]
        public List<Grade> GetGrades()
        {
            List<Grade> grades = new List<Grade>();

            //string strconn = "Data Source=" + HttpRuntime.AppDomainAppPath.Replace("\\", "/") + "/db/UV.db;Version=3;";
            System.Diagnostics.Debug.WriteLine(strconn);


            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();



                string qry = "SELECT * FROM Grades;";
                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {

                    SQLiteDataReader rd1 = cmd.ExecuteReader();
                    DataTable dt = new DataTable();

                    dt.Load(rd1);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            Grade g = new Grade();
                            g.ID = Convert.ToInt32(row["ID"]);
                            g.name = row["Name"].ToString();
                            g.years = Convert.ToInt32(row["Years"]);
                            grades.Add(g);
                        }

                    }


                } // End Query




                conn.Close();

            }//Connection end

            return grades;
        }


        [WebMethod]
        public Grade GetUserGrade(int ID)
        {
            Grade grade = new Grade();

            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                string qry = "SELECT Grades.* FROM Grades LEFT JOIN Users ON Users.ID_Grade = Grades.ID WHERE Users.ID = " + ID + ";";
                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {

                    SQLiteDataReader rd1 = cmd.ExecuteReader();
                    DataTable dt = new DataTable();

                    dt.Load(rd1);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow row = dt.Rows[0];
                        grade.ID = Convert.ToInt32(row["ID"]);
                        grade.name = row["Name"].ToString();
                        grade.years = Convert.ToInt32(row["Years"]);
                    }


                } // End Query




                conn.Close();

            }//Connection end

            return grade;
        }






        [WebMethod]
        public List<Subject> GetUserSubjects(int ID)
        {
            List<Subject> subjects = new List<Subject>();

            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                string qry = "SELECT Subjects.* FROM Subjects " +
                    "INNER JOIN Users_Subjects as us ON us.ID_subject = " +
                    "Subjects.ID WHERE us.ID_User =" + ID;

                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {


                    SQLiteDataReader rd1 = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(rd1);

                    foreach (DataRow row in dt.Rows)
                    {
                        Subject s = new Subject();
                        s.name = row["Name"].ToString();
                        s.ID = Convert.ToInt32(row["ID"]);
                        s.description = row["Description"].ToString();
                        s.year = Convert.ToInt32(row["Year"]);

                        subjects.Add(s);
                    }


                }


                conn.Close();
            }


            return subjects;
        }






        [WebMethod]
        public Subject GetSubject(int ID)
        {
            Subject s = new Subject();

            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                string qry = "SELECT Subjects.* FROM Subjects WHERE ID=" + ID;

                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {


                    SQLiteDataReader rd1 = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(rd1);

                    foreach (DataRow row in dt.Rows)
                    {
                        s.name = row["Name"].ToString();
                        s.ID = Convert.ToInt32(row["ID"]);
                        s.description = row["Description"].ToString();
                        s.year = Convert.ToInt32(row["Year"]);

                    }


                }


                conn.Close();
            }


            return s;
        }



        /// <summary>
        /// GET USERS DATA
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [WebMethod]
        public List<User> GetStudents()
        {
            List<User> users = new List<User>();

            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                string qry = "SELECT Users.* FROM USERS WHERE isTeacher = FALSE AND isAdmin = FALSE";

                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {


                    SQLiteDataReader rd1 = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(rd1);

                    foreach (DataRow row in dt.Rows)
                    {
                        User u = new User();
                        u.name = row["Name"].ToString();
                        u.surname = row["Surname"].ToString();
                        u.address = row["Address"].ToString();
                        u.userID = row["UserID"].ToString();
                        u.ID = Convert.ToInt32(row["ID"]);
                        u.pass = row["Pass"].ToString();
                        u.currentYear = Convert.ToInt32(row["CurrentYear"]);
                        u.ID_Grade = Convert.ToInt32(row["ID_Grade"]);
                        u.isTeacher = Convert.ToBoolean(row["isTeacher"]);
                        u.isAdmin = Convert.ToBoolean(row["isAdmin"]);

                        users.Add(u);


                    }


                }


                conn.Close();
            }


            return users;
        }


        /// <summary>
        /// ////////////////////?????
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [WebMethod]
        public List<String> GetSubjectsUser(int ID)
        {
            List<String> subjects = new List<String>();

            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                string qry = "SELECT Subjects.* FROM Subjects " +
                    "INNER JOIN Subject_Teacher as st ON st.ID_Subject = " +
                    "Subjects.ID WHERE st.ID_Teacher =" + ID;

                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {


                    SQLiteDataReader rd1 = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(rd1);

                    foreach (DataRow row in dt.Rows)
                    {
                        subjects.Add(row["Name"].ToString());
                    }


                }


                conn.Close();
            }


            return subjects;
        }


        /// <summary>
        /// USERS CRUD
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>



        [WebMethod]
        public bool AddUser(User u)
        {
            bool succed = false;
            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();


                using (MD5 md5Hash = MD5.Create())
                {
                    string psswd = u.pass;

                    byte[] pData = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(psswd));
                    //Convert the HEX bytes hassed array to string replacing '.', by empties
                    psswd = BitConverter.ToString(pData).Replace("-", String.Empty);


                    string qry = "INSERT INTO Users(Name, Surname, Address, ID_Grade, CurrentYear, UserID, Pass, isTeacher, isAdmin) " +
                        "VALUES (\"" + u.name + "\",\"" + u.surname + "\",\"" + u.address + "\", " + u.ID_Grade + ", " + u.currentYear + ", " +
                        "\"" + u.userID + "\", \"" + psswd + "\", " + u.isTeacher + ", " + u.isAdmin + ")";


                    using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                    {


                        using (SQLiteDataAdapter adapter = new SQLiteDataAdapter())
                        {
                            adapter.InsertCommand = cmd;
                            adapter.InsertCommand.ExecuteNonQuery();
                            succed = true;
                        }


                    }

                }

                conn.Close();
            }


            return succed;
        }



        [WebMethod]
        public bool EditUser(User u, bool changePass)
        {
            bool succed = false;
            string qry;

            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();


                string nPass = "";
                if (changePass)
                {
                    using (MD5 md5Hash = MD5.Create())
                    {
                        byte[] pData = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(u.pass));
                        String psswd = BitConverter.ToString(pData).Replace("-", String.Empty);

                        nPass = "Pass = \"" + psswd + "\", ";
                    }
                }


                qry = "UPDATE Users SET Name = \"" + u.name + "\", " +
                "Surname = \"" + u.surname + "\", " +
                "Address = \"" + u.address + "\", " +
                "ID_Grade = " + u.ID_Grade + ", " +
                "CurrentYear = " + u.currentYear + ", " +
                "UserID = \"" + u.userID + "\"," +
                nPass +
                "isTeacher = " + u.isTeacher + " " +
                "WHERE ID = " + u.ID;

                System.Diagnostics.Debug.WriteLine(qry);

                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {


                    using (SQLiteDataAdapter adapter = new SQLiteDataAdapter())
                    {
                        adapter.UpdateCommand = cmd;
                        adapter.UpdateCommand.ExecuteNonQuery();
                        succed = true;
                    }


                }



                conn.Close();
            }


            return succed;
        }



        [WebMethod]
        public bool DeleteUser(int ID)
        {
            bool succed = false;
            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();




                string qry = "DELETE FROM Users WHERE ID = " + ID;


                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {


                    using (SQLiteDataAdapter adapter = new SQLiteDataAdapter())
                    {
                        adapter.DeleteCommand = cmd;
                        adapter.DeleteCommand.ExecuteNonQuery();
                        succed = true;
                    }


                }



                conn.Close();
            }


            return succed;
        }



        /// <summary>
        /// SUBJECTS CRUD
        /// </summary>
        /// <param name="user_ID"></param>
        /// <param name="subject_ID"></param>
        /// <returns></returns>


        [WebMethod]
        public bool AddToSubject(int user_ID, int subject_ID)
        {
            bool succed = false;
            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                string qry = "INSERT INTO Users_Subjects(ID_User, ID_Subject) VALUES(" + user_ID + ", " + subject_ID + ");";


                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {


                    using (SQLiteDataAdapter adapter = new SQLiteDataAdapter())
                    {
                        adapter.InsertCommand = cmd;
                        adapter.InsertCommand.ExecuteNonQuery();
                        succed = true;
                    }


                }



                conn.Close();
            }


            return succed;
        }



        [WebMethod]
        public bool DeleteFromSubject(int uID, int sID)
        {
            bool succed = false;
            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                string qry = "DELETE FROM Users_Subjects WHERE ID_User = " + uID + " AND ID_Subject = " + sID;

                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {


                    using (SQLiteDataAdapter adapter = new SQLiteDataAdapter())
                    {
                        adapter.DeleteCommand = cmd;
                        adapter.DeleteCommand.ExecuteNonQuery();
                        succed = true;
                    }


                }



                conn.Close();
            }


            return succed;
        }




        /// <summary>
        /// DROP DOWNS
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>

        [WebMethod]
        public List<Subject> GetRemainingSubjectsForUser(int ID)
        {
            List<Subject> subjects = new List<Subject>();
            using (SQLiteConnection conn = new SQLiteConnection(strconn))
            {
                conn.Open();

                string qry = "SELECT Subjects.* FROM Subjects LEFT JOIN Users_Subjects as us ON us.ID_Subject = Subjects.ID " +
                    "LEFT JOIN Grades_Subjects ON Grades_Subjects.ID_subject = Subjects.ID " +
                    "LEFT JOIN Users ON Users.ID = us.ID_User " +
                    "WHERE Subjects.ID NOT IN (SELECT us2.ID_Subject FROM Users_Subjects as us2 WHERE us2.ID_User = " + ID + " ) AND " +
                    "Subjects.Year = (SELECT u2.CurrentYear FROM Users as u2 WHERE u2.ID = " + ID + ") AND " +
                    "Grades_Subjects.ID_grade = (SELECT u3.ID_grade FROM Users as u3 WHERE u3.ID = " + ID + ") " +
                    "GROUP BY Subjects.ID ";

                using (SQLiteCommand cmd = new SQLiteCommand(qry, conn))
                {

                    SQLiteDataReader rd1 = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(rd1);

                    foreach (DataRow row in dt.Rows)
                    {
                        Subject s = new Subject();
                        s.name = row["Name"].ToString();
                        s.ID = Convert.ToInt32(row["ID"]);
                        s.description = row["Description"].ToString();
                        s.year = Convert.ToInt32(row["Year"]);

                        subjects.Add(s);

                    }

                }



                conn.Close();
            }


            return subjects;
        }





        public class User
        {
            public int ID { get; set; }
            public String name { get; set; }
            public String surname { get; set; }
            public String userID { get; set; }
            public String pass { get; set; }
            public bool isTeacher { get; set; } = false;
            public String address { get; set; }
            public int ID_Grade { get; set; } = 0;
            public int currentYear { get; set; } = 0;
            public bool isAdmin { get; set; } = false;
        }



        public class Grade
        {
            public int ID { get; set; }
            public String name { get; set; }
            public int years { get; set; }

        }


        public class Subject
        {
            public int ID { get; set; }
            public String name { get; set; }
            public String description { get; set; }

            public int year { get; set; }
        }




    }



}
